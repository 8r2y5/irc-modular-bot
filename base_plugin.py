class BasePlugin(object):
    name = 'BasePlugin'
    version = '0.1'
    commands = {}
    auto_load = True

    def active(self):
        self.running = True
        return True

    def deactive(self):
        self.running = False
        return True

    def pubmsg(self, conn, event, sender, target, data):
        return True

    def privmsg(self, conn, event, sender, target, data):
        return True

    def join(self, conn, event, sender, target, data):
        return True

    def part(self, conn, event, sender, target, data):
        return  True

    def quit(self, conn, event, sender, target, data):
        return  True

    def namreply(self, conn, event, sender, target, data):
        return  True

    def welcome(self, conn, event, sender, target, data):
        return  True