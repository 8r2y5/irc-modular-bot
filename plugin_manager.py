import inspect
import logging
import os
import sys

from base_plugin import BasePlugin


class PluginManager(object):
    logger = logging.getLogger('ircbot.plugin_manager')
    plugins = {}
    commands = {'pubmsg': {}, 'privmsg': {}}

    def __init__(self, plugin_path, config, parent, plugin_class=BasePlugin):
        self.plugin_class = plugin_class
        self.get_handlers()
        self.config = config
        self.plugin_dir = os.path.join(os.path.abspath('.'), 'plugins')
        self.logger.debug(self.plugin_dir)
        self.parent = parent
        sys.path.append(self.plugin_dir)

    def load_plugins(self):
        files_in_plugin_dir = os.listdir(self.plugin_dir)
        seen = []

        for root, dirs, files in os.walk(self.plugin_dir):
            for _dir in dirs:
                # name = os.path.join(root, dir)
                name = _dir
                if not os.path.exists(os.path.join(self.plugin_dir, name)):
                    continue

                try:
                    mod = __import__(name, globals(), locals(), [], 0)

                    for _, plugin in inspect.getmembers(mod, inspect.isclass):

                        if issubclass(plugin, self.plugin_class) and \
                                                        plugin is not self.plugin_class and \
                                                        plugin not in seen:

                            plugin.logger = logging.getLogger(
                                'ircbot.plugins.%s' % plugin.name
                            )

                            plugin.plugin_manager = self
                            plugin.parent = self.parent
                            plugin.config = self.config

                            seen.append(plugin)

                except ImportError:
                    msg = 'Import error for %s' % name
                    self.logger.critical(msg)  # , exc_info=True)

                else:
                    cls = plugin()
                    self.plugins[cls.name] = cls

                    msg = 'Loaded %s' % name
                    self.logger.info(msg)

    def active_plugins(self):
        for name, cls in self.plugins.iteritems():
            for event, cmds in cls.commands.iteritems():
                for cmd in cmds:
                    self.commands[event][cmd] = getattr(cls, cmd)

            if cls.auto_load:
                cls.active()
                msg = 'Plugin %s is activated' % name
                self.logger.info(msg)

    def deactive_plugins(self):
        for name, cls in self.plugins.iteritems():
            if cls.running:
                cls.deactive()
                msg = 'Plugin %s is deactivated' % name
                self.logger.info(msg)

        self.commands = {'pubmsg': {}, 'privmsg': {}}

    def active_plugin(self, plugin_name):
        if plugin_name in self.plugins:
            cls = self.plugins[plugin_name]

            if cls.running:
                return 'It is already activated!'

            else:
                cls.active()

                for event, cmds in cls.commands.iteritems():
                    for cmd in cmds:
                        self.commands[event][cmd] = getattr(cls, cmd)

                self.logger.info('Activeted: %s' % plugin_name)
                return 'Plugin is activeted.'

        else:
            return 'There is no such plugin!'

    def deactive_plugin(self, plugin_name):
        if plugin_name in self.plugins:
            cls = self.plugins[plugin_name]

            if not cls.running:
                return 'It is already deactivated!'

            else:
                cls.deactive()

                for event, cmds in cls.commands.iteritems():
                    for cmd in cmds:
                        if cmd in self.commands[event]:
                            self.commands[event].pop(cmd)

                    else:
                        self.logger.warning('Event %s wasn\'t registered!' % cmd)

                self.logger.info('Deactiveted: %s' % plugin_name)
                return 'Plugin is deactiveted.'

        else:
            return 'There is no such plugin!'

    def unload_plugins(self):
        self.plugins = {}

    def parse_data(self, event, split=True):
        sender = event.source().split('!')[0]
        target = event.target()

        if target == self.config['name']:
            target = sender

        if event.arguments():
            if split:
                data = event.arguments()[0].split(' ')

            else:
                data = event.arguments()[0]
        else:
            data = []

        # msg = unicodedata.normalize(
        #     'NKFD', unicode(msg, 'utf-8')
        # ).encode('ascii','ignore').split(' ')

        return sender, target, data

    def deliver(self, conn, event, tell=False):
        sender, target, data = self.parse_data(event)
        if data and data[0].startswith(self.config['command prefix']):
            data[0] = data[0].lstrip(self.config['command prefix'])

            commands = self.commands.get(event.eventtype(), None)
            if not commands is None:
                if data[0] in commands:
                    f = commands[data[0]]
                    data = data[1:] if len(data) > 1 else []

                    try:
                        f(conn, event, sender, target, data)

                    except Exception, e:
                        self.logger.error(e, exc_info=True)

                elif tell:
                    conn.privmsg(target, 'There is no such command!')

    def do(self, conn, event, method):
        sender, target, data = self.parse_data(event)

        for name, plugin in self.plugins.iteritems():
            if plugin.running:
                f = getattr(plugin, method)
                f(conn, event, sender, target, data)

    def get_handlers(self):
        self.handlers = [
            name for name, _ in inspect.getmembers(BasePlugin, inspect.ismethod)
        ]

        for trash in ['__init__', 'active', 'deactive']:
            if trash in self.handlers:
                self.handlers.remove(trash)

    @staticmethod
    def do_sequence(functions):
        for func in functions:
            func()


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    p = PluginManager('.', {}, None)
    p.load_plugins()
    p.active_plugins()
    print p.plugins
    p.deactive_plugins()
