from functools import partial
from base_plugin import BasePlugin


class OwnerCommands(BasePlugin):
    name = 'owner_commands'

    def privmsg(self, conn, event, sender, target, data):
        if self.config['owner'] == sender:
            if data[0] == 'restart':
                self.parent.restart()

            elif data[0] == 'ppl':
                conn.privmsg(sender, 'List: %s.' % ', '.join(self.parent.ppl))

            elif data[0] == 'quit':
                self.parent.quit()

            elif data[0] == 'whois':
                conn.whois([data[1]])

            elif data[0] == 'who':
                conn.who(data[1])

            elif data[0] == 'det':
                conn.privmsg(sender, str(self.detailed))

            elif data[0] == 'plugins':
                plugins = [
                    (name, bool(plugin.running))
                    for name, plugin in self.plugin_manager.plugins.iteritems()
                ]
                active_plugins = []
                for name, active in plugins[:]:
                    if active:
                        active_plugins.append(name)
                        plugins.remove((name, active))

                conn.privmsg(sender, 'Active: %s.' % ', '.join(active_plugins))
                if plugins:
                    conn.privmsg(
                        sender, 'Deactive: %s.' % ', '.join(
                            [name for name, _ in plugins]
                        )
                    )

            elif data[0] == 'active':
                msg = self.plugin_manager.active_plugin(data[1])
                conn.privmsg(sender, msg)

            elif data[0] == 'deactive':
                msg = self.plugin_manager.deactive_plugin(data[1])
                conn.privmsg(sender, msg)

            elif data[0] == 'commands':
                for name, plugin in self.plugin_manager.plugins.iteritems():
                    if plugin.running:
                        msg = [
                            'PubMSG: %s.' % ', '.join(plugin.commands.get('pubmsg', [])),
                            'PrivMSG: %s.' % ', '.join(plugin.commands.get('privmsg', []))
                        ]
                        conn.privmsg(sender, '%s: %s' % (name, ' '.join(msg)))

            elif data[0] == 'reload':
                functions = [
                    partial(self.plugin_manager.deactive_plugins),
                    partial(self.plugin_manager.unload_plugins),
                    partial(self.plugin_manager.load_plugins),
                    partial(self.plugin_manager.active_plugins)
                ]
                self.plugin_manager.do_sequence(functions)