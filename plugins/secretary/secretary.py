import json
import base_plugin


class Secretary(base_plugin.BasePlugin):
    name = 'secretary'
    commands = {'privmsg': ['msg']}

    def active(self):
        self.db_file = 'messages.json'
        self.load_messages()
        super(Secretary, self).active()

    def msg(self, conn, event, sender, target, data):
        if data:
            if data[0] == 'check':
                self.msg_check(sender, conn)

            elif data[0] == 'send':
                self.msg_send(sender, data, conn)

            elif data[0] == 'history':
                self.msg_history(sender, conn)

            elif data[0] == 'stop':
                self.msg_stop(sender, conn)

            elif data[0] == 'start':
                self.msg_start(sender, conn)

            elif data[0] == 'help':
                self.msg_help(sender, conn)

            else:
                self.dont_get_it(conn, sender)

        else:
            self.msg_help(sender, conn)

    @staticmethod
    def msg_help(target, conn):
        msgs = [
            'msg send nick message - to leave message for the nick',
            'msg check - to check for new messages',
            'msg history - to get all messages',
            'msg stop - if you don\'t want to receive any messages in a future',
            'msg start - if you want to receive messages if you stopped it'
        ]

        for msg in msgs:
            conn.privmsg(target, msg)

    def msg_start(self, sender, conn):
        if sender in self.db['stopped']:
            self.db['stopped'].remove(sender)
            self.save_messages()
            conn.privmsg(sender, 'ok')

        else:
            conn.privmsg(sender, 'You already did that.')

    def msg_stop(self, sender, conn):
        if sender not in self.db['stopped']:
            self.db['stopped'].append(sender)
            self.save_messages()
            conn.privmsg(sender, 'ok')
        else:
            conn.privmsg(sender, 'You already did that!')

    def msg_history(self, sender, conn):
        if self.db['messages'].get(sender, False):
            was = False

            for nr in self.db['messages'][sender].keys():
                if not self.db['messages'][sender][nr]['delivered']:
                    self.db['messages'][sender][nr]['delivered'] = True

                was = True
                msg = '%s - %s' % (
                    str(nr), self.db['messages'][sender][nr]['msg'])
                conn.privmsg(sender, msg)

            if was:
                self.save_messages()

            else:
                conn.privmsg(sender, 'There is no new messages for you.')

        else:
            conn.privmsg(sender, 'There is no messages for you.')

    def msg_send(self, sender, msg, conn):
        if len(msg) > 2:
            nick = msg[1]

            if nick not in self.db['stopped']:
                message = ' '.join(msg[2:])
                nr = max(map(int, self.db['messages'].get(nick, {0: 0}).keys())) + 1
                tmp = self.db['messages'].get(nick, {})
                tmp[nr] = {
                    'msg': '%s sends: %s' % (sender, message),
                    'delivered': False
                }

                self.db['messages'][nick] = tmp
                self.save_messages()
                conn.privmsg(sender, 'ok')

            else:
                conn.privmsg(sender, 'That person doesn\'t want to receive messages.')

        else:
            conn.privmsg(
                sender, 'msg send nick message - to leave message for the nick')

    def msg_check(self, sender, conn):
        if self.db['messages'].get(sender, False):
            was = False

            for nr in self.db['messages'][sender].keys():
                if not self.db['messages'][sender][nr]['delivered']:
                    was = True
                    msg = '%s - %s' % (
                        str(nr), self.db['messages'][sender][nr]['msg'])
                    conn.privmsg(sender, msg)
                    self.db['messages'][sender][nr]['delivered'] = True

            if was:
                self.save_messages()

            else:
                conn.privmsg(sender, 'There is no new messages for you.')

        else:
            conn.privmsg(sender, 'There is no messages for you.')

    @staticmethod
    def dont_get_it(conn, sender):
        conn.privmsg(sender, 'I don\'t understand that, type msg help')

    def load_messages(self):
        try:
            self.db = json.load(open(self.db_file))

        except:
            self.db = {'messages': {}, 'stopped': []}
            self.save_messages()

    def save_messages(self):
        json.dump(self.db, open(self.db_file, 'wb'), indent=4, sort_keys=True)

    def handle_join(self, conn, event, sender, target, data):
        if sender not in self.db['stopped'] and self.db['messages'].get(sender, False):
            count = 0
            for nr in self.db['messages'][sender].keys():
                if not self.db['messages'][sender][nr]['delivered']:
                    count += 1

            if count > 0:
                conn.privmsg(
                    sender,
                    'There are %s new messages for you! Type msg check to read them!' \
                    % str(count)
                )