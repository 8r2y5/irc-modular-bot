from base_plugin import BasePlugin


class Logger(BasePlugin):
    name = 'logger'

    def pubmsg(self, conn, event, sender, target, data):
        self.logger.info('%s -> %s: %s' % (sender, target, data))

    def privmsg(self, conn, event, sender, target, data):
        self.logger.info('**%s: %s' % (sender, data))