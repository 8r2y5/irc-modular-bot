import urllib2
from base_plugin import BasePlugin


class UrlTitle(BasePlugin):
    name = 'url_title'

    def active(self):
        self.opener = urllib2.build_opener()
        self.opener.addheaders = [('User-agent', 'Mozilla/5.0')]
        super(UrlTitle, self).active()

    def pubmsg(self, conn, event, sender, target, data):
        data = ' '.join(data)
        if data.find('http') != -1:
            for url in data.split(' '):
                if url.startswith('http'):
                    html = self.opener.open(url).read()

                    if html.find('<title>') != -1:
                        title = html[html.find('<title>')+len('<title>'):html.find('</title>')]
                        conn.privmsg(target, '%s (%s)' % (title, url))