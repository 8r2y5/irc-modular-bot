import re, urllib
from base_plugin import BasePlugin


class Conv(BasePlugin):
    name = 'clever_bot'
    regex = ('vText%s|' * 7) % tuple(range(2, 9)) + 'conv_id|prevref'
    regex = re.compile('NAME=(%s) TYPE=hidden VALUE="([^"]*)"' % regex, re.M)
    data = {}

    def speak(self, text=''):
        self.data['vText1'] = text
        html = urllib.urlopen('http://jabberwacky.com/',
                              urllib.urlencode(self.data)).read()
        self.data = dict(self.regex.findall(html))
        return self.data['vText2']

    def pubmsg(self, conn, event, sender, target, data):
        if data[0].startswith(self.config['name']):
            msg = self.speak(' '.join(data[1:])).split(' ')
            msg[0] = msg[0].lower()
            conn.privmsg(target, '%s, %s' % (sender, ' '.join(msg)))