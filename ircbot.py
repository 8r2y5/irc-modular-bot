import json
import os
from random import randint
import time
import sys
import logging
import weakref

import irclib
from plugin_manager import PluginManager


class IrcHanlder(logging.Handler):
    def __init__(self, conn):
        logging.Handler.__init__(self)
        self.conn = conn

    def emit(self, record):
        try:
            self.conn(
                record.message
            )
        except:
            pass


class IrcSecretary(object):
    file_name = 'bot_config.json'
    running = False
    logger = logging.getLogger('ircbot')

    detailed = {}

    def parse_data(self, event, split=True):
        sender = event.source().split('!')[0]
        target = event.target()

        if target == self.config['name']:
            target = sender

        if split:
            data = event.arguments()[0].split(' ')

        else:
            data = event.arguments()[0]

        # msg = unicodedata.normalize(
        #     'NKFD', unicode(msg, 'utf-8')
        # ).encode('ascii','ignore').split(' ')

        return sender, target, data

    def __init__(self):
        self.logger.debug('Loading config')
        try:
            self.config = json.load(open(self.file_name))

        except:
            self.logger.error('Couldn\'t load config')
            self.config = {
                'server': '',
                'port': 6667,
                'channel name': '',
                'channel password': '',
                'name': 'Secretary',
                'indentify password': '',
                'owner': '',
                'command prefix': '!'
            }
            json.dump(self.config, open(self.file_name, 'wb'), indent=4, sort_keys=True)

        else:
            self.running = True
            self.ppl = []
            self.logger = self.logger.getChild(self.config['name'])
            self.logger = self.logger.getChild(
                self.config['channel name'].replace('#', ''))
            self.logger.debug('Config loaded')

    def add_handler(self, handler):
        h = IrcHanlder(
            lambda x: self.server.privmsg(self.config['owner'], x)
        )
        h.setLevel(logging.WARNING)
        handler.addHandler(h)

    def store_handler(self, handler):
        self._handler = handler

    def restart(self):
        self.server.quit('Restarting...')
        self.server.close()

        self.logger.info('Restarting')
        args = sys.argv[:]
        args.insert(0, sys.executable)
        os.execv(sys.executable, args)

    def wait(self, _max=3):
        wait = randint(1, _max) + (randint(1, 9) / 10.0) + (randint(1, 9) / 100.0)
        self.logger.debug('Wating %s' % (str(wait)))
        time.sleep(wait)

    def handler_welcome_msg(self, conn, event):
        self.add_handler(self._handler)
        self.logger.debug('A Welcome Message!')
        self.wait()

        if self.config['indentify password']:
            conn.privmsg('NickServ', 'IDENTIFY %s' % self.config['indentify password'])
            self.wait()

        self.join_channel(conn)
        self.p.deliver(conn, event, 'welcome')

    def join_channel(self, conn):
        self.logger.debug('Joinning %s' % self.config['channel name'])
        msg = self.config['channel name']

        if not msg.startswith('#'):
            msg = '#' + msg

        if self.config['channel password']:
            self.logger.debug('Adding password')
            msg += ' %s' % self.config['channel password']

        conn.join(msg)

    def handler_quit(self, conn, event):
        self.logger.debug('A quit!')
        sender = event.source().split('!')[0]
        self.remove_ppl(sender)
        self.p.deliver(conn, event, 'quit')

    def handler_part(self, conn, event):
        self.logger.debug('A part!')
        sender = event.source().split('!')[0]
        self.remove_ppl(sender)
        self.p.deliver(conn, event, 'part')

    def handler_join(self, conn, event):
        self.logger.debug('A join message!')
        sender = event.source().split('!')[0]
        self.add_ppl(sender)
        self.p.deliver(conn, event, 'join')

    def handler_name(self, conn, event):
        self.logger.debug('A name message')
        for nick in event.arguments()[2].split(' '):
            if nick.startswith('@'):
                nick = nick.lstrip('@')

            if nick != self.config['name'] and nick not in self.ppl:
                self.ppl.append(nick)

            self.logger.warning('%s was in ppl list!' % nick)

        self.logger.debug(str(self.ppl))
        self.p.deliver(conn, event, 'namreply')

    def handler_pubmsg(self, conn, event):
        # self.logger.info('pub msg')
        if event.arguments()[0].startswith(self.config['command prefix']):
            self.p.deliver(conn, event)
        self.p.do(conn, event, 'pubmsg')

    def handler_privmsg(self, conn, event):
        # self.logger.debug('A privmsg!!')
        # sender, target, msg = self.parse_data(event)

        if event.arguments()[0].startswith(self.config['command prefix']):
            self.p.deliver(conn, event)
        self.p.do(conn, event, 'privmsg')

    def handle_who_is_user(self, conn, event):
        self.logger.debug('whois user')
        temp_db = dict(zip(['nick', 'identity', 'address', 'i dont know', 'real name'], event.arguments()))
        self.logger.debug(str(temp_db))
        nick = temp_db.pop('nick')
        if nick not in self.detailed:
            self.detailed[nick] = temp_db

        else:
            for k, v in temp_db.iteritems():
                self.detailed[nick][k] = v

    def handle_who_is_channels(self, conn, event):
        self.logger.debug('whois channels')
        nick, channels = event.arguments()
        if nick not in self.detailed:
            self.detailed[nick] = {
                'channels': channels.split(' ')
            }
        elif 'channels' not in self.detailed[nick]:
            self.detailed[nick]['channels'] = channels.split(' ')

        else:
            for channel in channels:
                if not channel in self.detailed[nick]['channels']:
                    self.detailed[nick]['channels'].append(channel)

    def run(self):
        if self.running:
            self.p = PluginManager('.', self.config, weakref.proxy(self))
            self.p.load_plugins()
            self.p.active_plugins()

            irc = irclib.IRC()
            self.server = irc.server()

            irc.add_global_handler('welcome', self.handler_welcome_msg)
            irc.add_global_handler('privmsg', self.handler_privmsg)
            irc.add_global_handler('pubmsg', self.handler_pubmsg)
            irc.add_global_handler('quit', self.handler_quit)
            irc.add_global_handler('part', self.handler_part)
            irc.add_global_handler('namreply', self.handler_name)
            irc.add_global_handler('join', self.handler_join)
            irc.add_global_handler('whoisuser', self.handle_who_is_user)
            irc.add_global_handler('whoischannels', self.handle_who_is_channels)
            irc.add_global_handler('error', self.error)

            self.server.connect(
                self.config['server'],
                self.config['port'],
                self.config['name'],
                ircname='%s\'s bot' % self.config['owner']
            )
            irc.process_forever()

    def add_ppl(self, sender):
        if sender != self.config['name'] and sender not in self.ppl:
            self.ppl.append(sender)

        else:
            self.logger.warning('%s was in ppl list!' % sender)

    def remove_ppl(self, sender):
        if sender in self.ppl:
            self.ppl.remove(sender)

        else:
            self.logger.warning('%s want\' in the ppl list!')

    @staticmethod
    def dont_get_it(conn, sender):
        conn.privmsg(sender, 'I don\'t understand that, type help')

    def quit(self):
        self.p.deactive_plugins()
        del self.p

        self.logger.info('Closing')
        self.server.quit('Bye bye')
        self.server.close()

        del self.server
        sys.exit(0)

    def error(self, *args):
        self.quit()


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger()
    # s = logging.StreamHandler()
    # s.setLevel(logging.DEBUG)
    # logger.addHandler(s)
    irclib.DEBUG = True
    bot = IrcSecretary()
    bot.store_handler(logger)
    bot.run()